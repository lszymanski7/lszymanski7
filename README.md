<!-- ABOUT ME -->
## 👋🏻 Hello GitHub!
I became fascinated with programming over a decade ago. Since then, I have been exposed to many technologies and tools that are widely used by developers. The experience I gained helped me choose the direction of further professional development. I decided to focus mainly on web technologies. Designing and creating web applications has become my passion, providing me not only with great satisfaction, but also with motivation to constantly improve and search for innovative solutions in this dynamically changing IT area.

I am currently looking for an internship/job as a Front-end Developer to gain valuable professional experience and expand my knowledge of web technologies. In my daily work, I can offer: <br/>
✔ creating high-quality source code, <br/>
✔ ambitious pursuit of set goals, <br/>
✔ responsibility and commitment, <br/>
✔ teamwork skills, <br/>
✔ comfort in using foreign languages.

Interests play an important role in my life, perfectly harmonizing with my chosen professional path. I actively follow the latest information in the fields of technology and astronomy. Additionally, during my free time, I engage in sports and travel, which not only helps me maintain good physical and mental condition but also provides energy for effective work.

<br/>

<!-- TECHNOLOGY STACK -->
## 🛠️ Technology Stack
In my projects, I mostly use the following programming languages and tools:

![JavaScript](https://img.shields.io/badge/JavaScript-555555?style=flat&logo=javascript&logoColor=F7DF1E)
![React](https://img.shields.io/badge/React-555555?style=flat&logo=react&logoColor=61DAFB)
![Redux](https://img.shields.io/badge/Redux-555555?style=flat&logo=redux&logoColor=916EC9)
![HTML](https://img.shields.io/badge/HTML-555555?style=flat&logo=html5&logoColor=E34F26)
![CSS](https://img.shields.io/badge/CSS-555555?style=flat&logo=css3&logoColor=1572B6)
![Sass](https://img.shields.io/badge/Sass-555555?style=flat&logo=Sass&logoColor=CC6699)
![Styled Components](https://img.shields.io/badge/Styled%20Components-555555?style=flat&logo=styledcomponents&logoColor=DB7093)
![React Router](https://img.shields.io/badge/React%20Router-555555?logo=react-router&logoColor=CA4245)
![Webpack](https://img.shields.io/badge/Webpack-555555?style=flat&logo=webpack&logoColor=8DD6F9)
![Babel](https://img.shields.io/badge/Babel-555555?style=flat&logo=babel&logoColor=F9DC3E)
![Jest](https://img.shields.io/badge/Jest-555555?style=flat&logo=jest&logoColor=C21325)
![Testing Library](https://img.shields.io/badge/Testing%20Library-555555?style=flat&logo=testinglibrary&logoColor=E33332)
![Git](https://img.shields.io/badge/Git-555555?style=flat&logo=git&logoColor=F05032)
![Npm](https://img.shields.io/badge/Npm-555555?style=flat&logo=npm&logoColor=CB3837)
![ESLint](https://img.shields.io/badge/ESLint-555555?style=flat&logo=eslint&logoColor=4B32C3)
![Prettier](https://img.shields.io/badge/Prettier-555555?style=flat&logo=prettier&logoColor=F7B93E)
![Firebase](https://img.shields.io/badge/Firebase-555555?style=flat&logo=firebase&logoColor=FFCA28)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-555555?style=flat&logo=visual%20studio%20code&logoColor=007ACC)

I'm currently learning:

![TypeScript](https://img.shields.io/badge/TypeScript-555555?style=flat&logo=typescript&logoColor=3178C6)
![Material UI](https://img.shields.io/badge/Material%20UI-555555?style=flat&logo=mui&logoColor=007FFF)
![Redux-Saga](https://img.shields.io/badge/Redux--Saga-555555?style=flat&logo=reduxsaga&logoColor=86D46B)
![Cypress](https://img.shields.io/badge/Cypress-555555?style=flat&logo=cypress&logoColor=FFFFFF)

<br/>

<!-- LINKS -->
##
<div align="center">
  <p>If you have any questions or suggestions, please send me an <a href="mailto:lszymanski.info@gmail.com?subject=GitLab - Your subject here...">e-mail</a>. <br/> You can also find me on the following websites:</p>
  
  [![Stack Overflow](https://img.shields.io/badge/Stack%20Overflow-F47F24?style=flat&logo=stackoverflow&logoColor=white)](https://stackoverflow.com/users/18706083)
  [![GitHub](https://img.shields.io/badge/GitHub-555555?style=flat&logo=github)](https://github.com/lszymanski7)
  [![LinkedIn](https://img.shields.io/badge/LinkedIn-0A66C2?style=flat&logo=linkedin)](https://linkedin.com/in/lszymanski7)
</div>
